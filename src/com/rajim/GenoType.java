package com.rajim;


import java.io.Serializable;

public class GenoType implements Serializable {

    private double[] gens;
    private double fitness;
    private double[] upper;
    private double[] lower;
    private double relativeFitness;
    private double cumulativeFitness;

//    public GenoType() {
//        for (int i = 0; i < 3; i++) {
//            this.gens[i] = 0;
//            this.upper[i] = 0;
//            this.lower[i] = 0;
//        }
//    }

    @Override
    protected GenoType clone()  {
        try {
            return (GenoType) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String toString() {
        return "Gens::"+this.getGens()[0]+", "+this.getGens()[1]+", "+this.getGens()[0]+" and fitness::"+this.getFitness();
    }

    public GenoType(double[] gens,
                    double fitness,
                    double[] upper,
                    double[] lower,
                    double relativeFitness,
                    double cumulativeFitness) {

//        for (int i = 0; i < 3; i++) {
//            this.gens[i] = gens[i];
//            this.upper[i] = upper[i];
//            this.lower[i] = lower[i];
//        }
        this.gens = gens;
        this.upper = upper;
        this.lower = lower;
        this.fitness = fitness;
        this.relativeFitness = relativeFitness;
        this.cumulativeFitness = cumulativeFitness;
    }

    public double[] getGens() {
        return gens;
    }

    public void setGens(double[] gens) {
        this.gens = gens;
    }

    public double getFitness() {
        return fitness;
    }

    public void setFitness(double fitness) {
        this.fitness = fitness;
    }

    public double[] getUpper() {
        return upper;
    }

    public void setUpper(double[] upper) {
        this.upper = upper;
    }

    public double[] getLower() {
        return lower;
    }

    public void setLower(double[] lower) {
        this.lower = lower;
    }

    public double getRelativeFitness() {
        return relativeFitness;
    }

    public void setRelativeFitness(double relativeFitness) {
        this.relativeFitness = relativeFitness;
    }

    public double getCumulativeFitness() {
        return cumulativeFitness;
    }

    public void setCumulativeFitness(double cumulativeFitness) {
        this.cumulativeFitness = cumulativeFitness;
    }

}
